; Yannela Castro
; 13-07-2020
%macro imprimir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80H
%endmacro

%macro leer 2
	mov eax, 3
	mov ebx, 0
	mov ecx, %1
	mov edx, %2
	int 80H
%endmacro

section .data
	msg1		db		10,'-Calculadora-',10,0
	lmsg1		equ		$ - msg1
 
	msg2		db		10,'Numero 1: ',0
	lmsg2		equ		$ - msg2
 
	msg3		db		'Numero 2: ',0
	lmsg3		equ		$ - msg3

	msg4		db		10,'1. Sumar',10,0
	lmsg4		equ		$ - msg4

	msg5		db		'2. Restar',10,0
	lmsg5		equ		$ - msg5

	msg6		db		'3. Multiplicar',10,0
	lmsg6		equ		$ - msg6

	msg7		db		'4. Dividir',10,0
	lmsg7		equ		$ - msg7

	msg11		db		'5. Salir',10,0
	lmsg11		equ		$ - msg11

	msg8		db		'Seleccione la opción',10,0
	lmsg8		equ		$ - msg6

	msg9		db		10,'Resultado: ',0
	lmsg9		equ		$ - msg9
 
	msg10		db		10,'Opcion Invalida',10,0
	lmsg10		equ		$ - msg10
 
	nlinea		db		10,10,0
	lnlinea		equ		$ - nlinea
 
section .bss
	opcion:		resb	2
  	num1:		resb	2
	num2:		resb 	2
	resultado:	resb 	2
 
section .text
 	global _start
 
_start:
	;***********título o encabezado del ejercicio***************
	imprimir msg1, lmsg1
 
	;********** mensaje del primer número ***********
	imprimir msg2, lmsg2
	leer num1, 2

	;********** mensaje del segundo número ***********
	imprimir msg3, lmsg3
	leer num2, 2
 
	;********** mensaje del menu ***********
	imprimir msg4, lmsg4	;sumar
	imprimir msg5, lmsg5	;restar
	imprimir msg6, lmsg6	;multiplicar
	imprimir msg7, lmsg7	;dividir
	imprimir msg11, lmsg11	;salir

opciones:
	imprimir msg8, lmsg8
	leer opcion, 2

	mov ah, [opcion]
	sub ah, '0'
	;comparar
	cmp ah, 1
	JE sumar

	cmp ah, 2
	JE restar

	cmp ah, 3
	JE multiplicar

	cmp ah, 4
	JE dividir

	cmp ah, 5
	JE salir
 
	; Si el valor ingresado no cumple con ninguna de las condiciones anteriores entonces mostramos un mensaje de error y finalizamos
	; la ejecucion del programa
	imprimir msg10, lmsg10
 
 sumar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Sumamos el registro AL y BL
	add al, bl
;	aaa
 
	; Convertimos el resultado de la suma de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	imprimir msg9, lmsg9
	imprimir resultado, 2
	
	jmp opciones
 
restar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Restamos el registro AL y BL
	sub al, bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al

	imprimir msg9, lmsg9
	imprimir resultado, 2
 
	jmp opciones
 
multiplicar:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Multiplicamos. AX = AL X BL
	mul bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	imprimir msg9, lmsg9
	imprimir resultado, 2

	jmp opciones
 
dividir:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Igualamos a cero los registros DX y AH
	mov dx, 0
	mov ah, 0
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Division. AL = AX / BL. AX = AH:AL
	div bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
	
	imprimir msg9, lmsg9
	imprimir resultado, 2

	jmp opciones
 
salir:
	imprimir nlinea, nlinea
 
	; Finalizamos el programa
	mov eax, 1
	mov ebx, 0
	int 80h
