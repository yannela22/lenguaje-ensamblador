section .data
	nombres DB "Yannela Mishelle", 10
	len_nombres EQU $-nombres

	apellidos DB "Castro Valarezo", 10
	len_apellidos EQU $-apellidos

	materia DB "Lenguaje Ensamblador", 10
	len_materia EQU $-materia

	genero DB "F", 10
	len_genero EQU $-genero

section .text
	global _start
_start:
	mov eax, 4
	mov ebx, 1
	mov ecx, nombres
	mov edx, len_nombres
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, apellidos
	mov edx, len_apellidos
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, materia
	mov edx, len_materia
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, genero
	mov edx, len_genero
	int 80H

	mov eax, 1
	mov ebx, 0
	int 80H
