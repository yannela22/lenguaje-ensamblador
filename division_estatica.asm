;Dividir dos números estáticos
;Yannela Castro

section .data
	resultado db 'El resultado de la division es:', 10
	len equ $-resultado

	resultado_r db 10, 'El residuo es:'
	len_r equ $-resultado_r

	esp_enter db "", 10
	len_esp_enter equ $-esp_enter

section .bss
	cociente resb 1
	residuo resb 1

section .text
	global _start
_start:
	;al es el cociente
	;ah es el residuo

	mov al, 9
	mov bh, 6						
	div bh			
	add al, '0'
	mov [cociente], al
	add ah, '0'
	mov [residuo], ah

	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, len
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, cociente
	mov edx, 1
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, resultado_r
	mov edx, len_r
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, residuo
	mov edx, 1
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, esp_enter
	mov edx, len_esp_enter
	int 80H

	mov eax, 1
	int 80H
