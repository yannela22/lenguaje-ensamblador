;Yannela Castro

section .data
	mensaje db 'Ingrese un numero', 10
	len_mensaje equ $-mensaje
	mensaje_presentacion db 'El numero ingresado es', 10
	len_mensaje_presentacion equ $-mensaje_presentacion
section .bss
	numero resb 5		;reserva 5 espacios
section .text
	global _start
_start:
	;************************** imprime mensaje *******************************
	mov eax, 4
	mov ebx, 1
	mov ecx, mensaje
	mov edx, len_mensaje
	int 80H
	;************************** lectura de numero *******************************

	mov eax, 3		;define el tipo de operacion
	mov ebx, 2		;estándar de entrada
	mov ecx, numero		;lo que captura en el teclado
	mov edx, 5		;número de caracteres(debe coincidir con los valores que está reservado en la declaracion de la variable (.bss))
	int 80H			; interrupción de gnu linux
	
	;************************** imprime mensaje presentacion *******************************
	mov eax, 4
	mov ebx, 1
	mov ecx, mensaje_presentacion
	mov edx, len_mensaje_presentacion
	int 80H

	;************************** imprime numero *******************************
	mov eax, 4
	mov ebx, 1
	mov ecx, numero
	mov edx, 5
	int 80H

	;************************** imprime mensaje presentacion *******************************
	mov eax, 1
	int 80H
