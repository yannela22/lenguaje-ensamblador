%macro escribir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 0x80
%endmacro

section .data
    msj1 db "Ingrese datos en el archivo", 10
    len1 equ $-msj1

    archivo db "/home/yannela/Escritorio/ensamblador/archivo.txt"

section .bss
    texto resb 30
    idarchivo resd 1

section .txt
    global _start

    leer:
    mov eax, 3
    mov ebx, 0
    mov ecx, texto
    mov edx, 10
    int 80h
    ret

_start:
    mov eax, 8              ; [id], servicio para crear archivos, trabajar con archivos
    mov ebx, archivo        ;dirección del archivo
    mov ecx, 1              ; MODO DE ACCESO
                                ; O-RDONLY 0: El archivo se abre solo para leer
                                ; O-WRONLY 1: El archivo se abre para escritura
                                ; O-RDWR 2: El archivo se abre para lectura y escritura
                                ; O-CREATE 256: Crea el archivo en caso que no existe
                                ; O-APPEND 2000h: El archivo se abre solo para escritura al final
    mov edx, 777h
    int 80h

    test eax, eax           ; instruccion de comparación realiza la operacion logica "Y" de dos operandos,
                            ; pero NO afecta a ninguno de ellos, SOLO
                            ; todos los tipos de direccionamiento
                                ;TEST reg, reg
                                ;TEST reg, mem
                                ;TEST mem, reg
                                ;TEST reg, inmediato
                                ;TEST mem, inmediato

    jz salir                ; se ejecuta cuando existen errores en el archivo

    mov dword [idarchivo], eax
    escribir msj1, len1

    call leer

    ; ************* escritura en el archivo *************
    mov eax, 4
    mov ebx, [idarchivo]
    mov ecx, texto
    mov edx, 20
    int 0x80

salir:
    mov eax, 1
    int 80h