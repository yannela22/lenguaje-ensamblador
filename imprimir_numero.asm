;Yannela Castro

section .data
	mensaje db 'Ingrese un numero', 10
	len_mensaje equ $-mensaje
	mensaje_presentacion db 'El numero ingresado es', 10
	len_mensaje_presentacion equ $-mensaje_presentacion

section .bss
	numero resb 5
		
section .text
	global _start
_start:
	;************************** imprime mensaje *******************************
	mov eax, 4
	mov ebx, 1
	mov ecx, mensaje
	mov edx, len_mensaje
	int 80H

	;************************** asignacion de un numero en variable *******************************
	mov ebx, 7
	add ebx, '0'	
	mov [numero], ebx
	
	;************************** imprime mensaje presentacion *******************************
	mov eax, 4
	mov ebx, 1
	mov ecx, mensaje_presentacion
	mov edx, len_mensaje_presentacion
	int 80H

	;************************** imprime numero *******************************
	mov eax, 4
	mov ebx, 1
	mov ecx, numero
	mov edx, 5
	int 80H

	;************************** imprime mensaje presentacion *******************************
	mov eax, 1
	int 80H
