;Yannela Castro
;29/07/2020
;Ejemplo LOOP

section .data
    saludo db 'Hola',10
    len_saludo equ $-saludo

section .txt
    global _start

_start:
    mov ecx, 10

for:
    push rcx        ;push - pop para no perder el valor de ecx
    mov eax, 4
    mov ebx, 1
    mov ecx, saludo     ;el valor debe ser numerico para decrementar
    mov edx, len_saludo
    int 80h
    pop rcx
    loop for        ;por cada ciclo dec cx
salir:
    mov eax, 1
    int 80h
