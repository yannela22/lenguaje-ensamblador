;Yannela Castro
;29/07/2020
;Ejercicio LOOP-item

section .data
    saludo db 10, 'Hola: linea '
    len_saludo equ $-saludo

    esp_enter db "", 10
	len_esp_enter equ $-esp_enter

section .bss
    item resb 1

section .txt
    global _start

_start:
    mov ecx, 9

for:
    push rcx        ;push - pop para no perder el valor de ecx

    add ecx, '0'
    mov [item], ecx
    
    mov eax, 4
    mov ebx, 1
    mov ecx, saludo     ;el valor debe ser numerico para decrementar
    mov edx, len_saludo
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, item
    mov edx, 1
    int 80h

    pop rcx
    loop for    ;por cada ciclo dec cx

    mov eax, 4
    mov ebx, 1
    mov ecx, esp_enter
    mov edx, len_esp_enter
    int 80h

salir:
    mov eax, 1
    int 80h