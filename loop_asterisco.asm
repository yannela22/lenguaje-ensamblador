;Yannela Castro
;29/07/2020
;Piramide de asteriscos-LOOP

section .data
        asterisco db '*'
        new_line db 10,''

section .text
        global _start
_start:
        mov ecx, 8
        mov ebx, 1

l1:
        push rcx
        push rbx
        mov ecx, ebx    ;sin esta linea el triangulo se imprime de forma inversa

l2:
        push rcx
        ;****************asteriscos****************
        mov eax, 4
        mov ebx, 1
        mov ecx, asterisco
        mov edx, 1
        int 80h
        pop rcx
        loop l2         ;salto a l2, dec cx

        ;****************nueva linea****************
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, 1
        int 80h

        pop rbx
        pop rcx
        inc rbx
        loop l1
        ;****************finalizo filas****************

        mov eax, 1
        int 80h
