;Yannela Castro
;03/08/2020
;Tabla de multiplicat

section .data
    msj1 db ' * '
    len_msj1 equ $-msj1

    msj2 db ' = '
    len_msj2 equ $-msj2
   
    esp_enter db '', 10
    len_esp_enter equ $-esp_enter

section .bss
	a resb 2
    b resb 2
    c resb 2

section .txt
    global _start

_start:
    mov al, 3
    add al, '0'
    mov [a], al
    mov cx, 1

ciclo:
    push cx
    mov ax, [a]
    sub ax, '0'
    mul cx
    add ax, '0'
    mov [c], ax
    add cx, '0'
    mov [b], cx
    call imprimir_numero1
    call asterisco
    call imprimir_numero2
    call igual
    call imprimir_rta
    call nueva_linea
    pop cx              ;rescatar valor de pila
    inc cx
    cmp cx, 10
    jnz ciclo
    
imprimir_numero1:
    mov eax, 4
    mov ebx, 1
    mov ecx, a
    mov edx, 1
    int 80h
    ret

asterisco:
    mov eax, 4
    mov ebx, 1
    mov ecx, msj1
    mov edx, len_msj1
    int 80h
    ret

imprimir_numero2:
    mov eax, 4
    mov ebx, 1
    mov ecx, b
    mov edx, 1
    int 80h
    ret

igual:
    mov eax, 4
    mov ebx, 1
    mov ecx, msj2
    mov edx, len_msj2
    int 80h
    ret

imprimir_rta:
    mov eax, 4
    mov ebx, 1
    mov ecx, c
    mov edx, 1
    int 80h
    ret

nueva_linea:
    mov eax, 4
    mov ebx, 1
    mov ecx, esp_enter
    mov edx, len_esp_enter
    int 80h
    ret

	mov eax, 1
	int 80H