;Multiplicación de dos números estáticos
;Yannela Castro

section .data
	resultado db 'El resultado es:', 10
	len equ $-resultado
	esp_enter db "", 10
	len_esp_enter equ $-esp_enter

section .bss
	multip resb 1

section .text
	global _start
_start:

	;*****Mult con ajuste *****
	mov al, 3						;mov eax 4
	mov bh, 2						;mov ebx 2
	mul bh		; eax = eax * ebx			;imul eax, ebx
	add al, '0'						;add al, '0'
	;****Mult "normal"****
	;mov eax, 3
	;mov ebx, 2
	;mul ebx		; eax = eax * ebx
	;add eax, '0'
	
	mov [multip], al					;mov[multip], eax

	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, len
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, multip
	mov edx, 1
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, esp_enter
	mov edx, len_esp_enter
	int 80H

	mov eax, 1
	int 80H
