;Operaciones con números estáticos
;Yannela Castro Valarezo

%macro imprimir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80H
%endmacro

section .data
	resultado db 'El resultado de la Suma es:', 10
	len equ $-resultado

	res_resta db 10, 'El resultado de la Resta es:', 10
	len_resta equ $-res_resta

	res_mult db 10, 'El resultado de la Multiplicacion es:', 10
	len_multip equ $-res_mult
	
	res_div db 10, 'El resultado de la Division es:', 10
	len_div equ $-res_div

	resultado_r db 10, 'El residuo es: '
	len_r equ $-resultado_r

	esp_enter db "", 10
	len_esp_enter equ $-esp_enter

section .bss
	suma resb 1
	resta resb 1
	multip resb 1
	cociente resb 1
	residuo resb 1

section .text
	global _start
_start:
	;*************** Suma ***************
	imprimir resultado, len

	mov al, 4
	mov bl, 2
	add al, bl
	add al, '0'

	mov [suma], al

	imprimir suma, 1

	;*************** Resta ***************
	imprimir res_resta, len_resta

	mov al, 4
	mov bl, 2
	sub al, bl	
	add al, '0'

	mov [resta], al
	
	imprimir resta, 1

	;*************** Multiplicación ***************
	imprimir res_mult, len_multip

	mov al, 4
	mov bl, 2						
	mul bl				
	add al, '0'

	mov [multip], al

	imprimir multip, 1

	;*************** División ***************
	imprimir res_div, len_div

	mov al, 5
	mov bl, 2						
	div bl			
	add al, '0'

	mov [cociente], al 
	add ah, '0'
	mov [residuo], ah

	imprimir cociente, 1
	imprimir resultado_r, len_r
	imprimir residuo, 1


	;***************************************************
	imprimir esp_enter, len_esp_enter

	mov eax, 1
	int 80H
	
