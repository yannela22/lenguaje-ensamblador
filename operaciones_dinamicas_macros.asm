; Yannela Castro
; 22-06-2020

%macro leer 2
	mov eax, 3
	mov ebx, 2
	mov ecx, %1
	mov edx, %2
	int 80H
%endmacro

%macro imprimir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80H
%endmacro

section .data
	n1 db 10, 'Numero 1: '
	len_n1 equ $-n1

	n2 db 10, 'Numero 2: '
	len_n2 equ $-n2

	resultado db 10, 'El resultado de la Suma es:', 10
	len equ $-resultado

	res_resta db 10, 'El resultado de la Resta es:', 10
	len_resta equ $-res_resta

	res_mult db 10, 'El resultado de la Multiplicacion es:', 10
	len_multip equ $-res_mult
	
	res_div db 10, 'El resultado de la Division es:', 10
	len_div equ $-res_div

	resultado_r db 10, 'El residuo es: '
	len_r equ $-resultado_r

	esp_enter db "", 10
	len_esp_enter equ $-esp_enter

section .bss
	a resb 1
	b resb 1
	suma resb 1
	resta resb 1
	multip resb 1
	cociente resb 1
	residuo resb 1

section .text
	global _start
_start:
	imprimir n1, len_n1
	leer a, 2
	imprimir n2, len_n2
	leer b, 2
	;*************** Suma ***************
	imprimir resultado, len

	mov al, [a]
	sub al, '0'		; valores convertidos en digitos
	mov bl, [b]
	sub bl, '0'
	add al, bl
	add al, '0'		; valor convertido a cadena
	
	mov [suma], al

	imprimir suma, 1
	;*************** Resta ***************
	imprimir res_resta, len_resta

	mov al, [a]
	sub al, '0'		
	mov bl, [b]
	sub bl, '0'
	sub al, bl	
	add al, '0'

	mov [resta], al
	
	imprimir resta, 1

	;*************** Multiplicación ***************
	imprimir res_mult, len_multip

	mov al, [a]
	sub al, '0'		
	mov bl, [b]
	sub bl, '0'						
	mul bl				
	add al, '0'

	mov [multip], al

	imprimir multip, 1

	;*************** División ***************
	imprimir res_div, len_div

	mov al, [a]
	sub al, '0'		
	mov bl, [b]
	sub bl, '0'						
	div bl			
	add al, '0'

	mov [cociente], al 
	add ah, '0'
	mov [residuo], ah

	imprimir cociente, 1
	imprimir resultado_r, len_r
	imprimir residuo, 1


	;***************************************************
	imprimir esp_enter, len_esp_enter

	mov eax, 1
	int 80H
