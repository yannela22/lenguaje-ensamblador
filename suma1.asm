;Suma de dos números estáticos, no se ingresará por teclado
;Yannela Castro

section .data
	resultado db 'El resultado es:', 10
	len equ $-resultado
	esp_enter db "", 10
	len_esp_enter equ $-esp_enter

section .bss
	suma resb 1

section .text
	global _start
_start:
	mov eax, 6
	mov ebx, 2
	add eax, ebx		; eax = eax + ebx
	add eax, '0'		; ajuste
	
	mov [suma], eax
	
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, len
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, suma
	mov edx, 1
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, esp_enter
	mov edx, len_esp_enter
	int 80H

	mov eax, 1
	int 80H
	
