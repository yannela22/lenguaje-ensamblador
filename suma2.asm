;Suma de dos números estáticos, ingresados por teclado
;Yannela Castro

section .data
	mensaje db 10, 'Numero 1:'
	len_mensaje equ $-mensaje

	msg db 10, 'Numero 2:'
	len_msg equ $-msg

	resultado db 'El resultado es:', 10
	len equ $-resultado

	esp_enter db "", 10
	len_esp_enter equ $-esp_enter

section .bss
	numeroa resb 1
	numerob resb 1
	suma resb 1

section .text
	global _start
_start:
	;************************** imprime mensaje *******************************
	mov eax, 4
	mov ebx, 1
	mov ecx, mensaje
	mov edx, len_mensaje
	int 80H
	;************************** lectura de numero 1 *******************************
	mov eax, 3		
	mov ebx, 2		
	mov ecx, numeroa		
	mov edx, 2
	int 80H
	;************************** imprime msg *******************************
	mov eax, 4
	mov ebx, 1
	mov ecx, msg
	mov edx, len_msg
	int 80H
	;************************** lectura de numero 2 *******************************
	mov eax, 3		
	mov ebx, 2		
	mov ecx, numerob		
	mov edx, 2
	int 80H
	
	mov eax, [numeroa]
	sub eax, '0'		; valores convertidos en digitos
	mov ebx, [numerob]
	sub ebx, '0'
	add eax, ebx
	add eax, '0'		; valor convertido a cadena
	
	mov [suma], eax
	
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, len
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, suma
	mov edx, 1
	int 80H

	mov eax, 4
	mov ebx, 1
	mov ecx, esp_enter
	mov edx, len_esp_enter
	int 80H

	mov eax, 1
	int 80H
