;Yannela Castro
;05/08/2020
;Tablas de multiplicar

section .data
    msj1 db ' * '
    len_msj1 equ $-msj1

    msj2 db ' = '
    len_msj2 equ $-msj2
    
    esp_enter db '', 10
    len_esp_enter equ $-esp_enter

section .bss
    a resb 3
    b resb 3
    c resb 2

section .text
        global _start

_start:
    mov dx, 1
    add dx, '0'
    mov [a], dx
    mov cx, 1
    jmp ciclo

fila:
    call nueva_linea
    mov dx, [a]         ;Almacena en dx el valor de a     
    inc dx     
    mov[a], dx 
    mov cx, 1           ;Reinicia cx
    cmp dx, '9'         ;Compara dx y 9
    jg salir  
    jnz ciclo 
    
ciclo:
    push cx
    mov ax, [a]
    sub ax, '0'
    mul cx
    add al, '0'
    add ah, '0'
    mov [c], ah        ;Almacena el valor de ah en la direccion de memoria 0 de la variable c   
    mov [c], al  
    add cx, '0'
    mov [b], cx        ;Mueve en valor de cx a b    
    call imprimir_numero1
    call asterisco
    call imprimir_numero2
    call igual
    call imprimir_rta
    call nueva_linea
    pop cx
    inc cx
    cmp cx, 10   
    jnz ciclo           ;Si no es igual repite el ciclo
    jz fila             ;Si es igual salta a fila

imprimir_numero1:
    mov eax, 4
    mov ebx, 1
    mov ecx, a
    mov edx, 1
    int 80h
    ret

asterisco:
    mov eax, 4
    mov ebx, 1
    mov ecx, msj1
    mov edx, len_msj1
    int 80h
    ret

imprimir_numero2:
    mov eax, 4
    mov ebx, 1
    mov ecx, b
    mov edx, 1
    int 80h
    ret

igual:
    mov eax, 4
    mov ebx, 1
    mov ecx, msj2
    mov edx, len_msj2
    int 80h
    ret

imprimir_rta:
    mov eax, 4
    mov ebx, 1
    mov ecx, c
    mov edx, 1
    int 80h
    ret

nueva_linea:
    mov eax, 4
    mov ebx, 1
    mov ecx, esp_enter
    mov edx, len_esp_enter
    int 80h
    ret

salir:
    mov eax, 1
    int 80h